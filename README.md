Description
---

Ce projet a été créé lors d'une activité organisé par CY Tech, où nous devions créer un jeu sur les femmes en science. Pour ce faire, j'avais décidé de créer un jeu vidéo avec Unity sur la chercheuse Emmanuelle Charpentier qui a entre autre mise au point les ciseaux moléculaires et a reçu un prix nobel pour cette découverte. 

Les modèles 2D ainsi que la programmation ont entièrement été réalisé par mes soins à l'aide du logiciel Aseprite pour les textures et Unity pour la programmation du jeu en lui même.

Le speech est le suivant : Vous incarnez Mme Charpentier dans un vaisseau miniature et vous avez comme but de défendre l'ADN de votre patient en détruisant les virus l'attaquant à l'aide dun canon à anticorps. Vous pourrez réparer l'ADN de votre patient si celui-ci est touché à l'aide de vos précieux ciseaux moléculaires que vous pourrez trouver lors de votre avancé dans le jeu.

Le jeu se présente comme une sorte de shoot'em ups avec une difficulté croissante au fur et à mesure du temps. Plusieurs modes de jeu sont disponibles, que ce soit différents mode de difficulté pour le mode de base, un mode avec une unique vie, un mode où il n'est pas possible de tirer et où le seul moyen de se débarasser des virus est de rentrer en collision avec eux, ainsi qu'un mode de jeu infini où le but est uniquement de tirer sur un maximum de virus.

Pour rendre le jeu plus intéressant, différent bonus sont trouvables et utilisables que ce soit une augmentation de la vitesse de tir ou un bombe antivirus détruisant tous les virus à l'écran. 

Le joueur peut également réalisé des achievements en jouant au jeu, ce qui donne un but en plus au fait de jouer.

--- 

Utilisation
---

L'éxecutable est présent dans le dossier "game exe". Il est nécessaire de télécharger le dossier en entier pour pouvoir faire lancer le jeu.

---

Copyright
---
Les musiques utilisés dans le jeu ne m'appartiennent pas.

Elevator Madness (w/ Caturday) by Babasmas | https://soundcloud.com/babasmasmoosic
Music promoted by https://www.chosic.com/free-music/all/
Creative Commons CC BY-SA 3.0
https://creativecommons.org/licenses/by-sa/3.0/



Gothic Dark by PeriTune | https://peritune.com/
Music promoted by https://www.chosic.com/free-music/all/
Creative Commons CC BY 4.0
https://creativecommons.org/licenses/by/4.0/





