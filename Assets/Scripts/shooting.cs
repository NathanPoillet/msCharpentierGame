using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooting : MonoBehaviour
{
    public Transform weapon;
    public GameObject anticorps;
    public AudioSource _audio;
    public AudioClip shoot;

    public float nextFire = 0.0F;
    public float fireRate = 0.35f;

    private int nombreMissileLances;

    private void Start()
    {
        nombreMissileLances = PlayerPrefs.GetInt("anticorpsFired");
        _audio =GetComponent<AudioSource>();
        _audio.volume = Difficulty.volume/2;
    }
    void Update()
    {

        if (Difficulty.difficulties != 7) { 
            if (Input.GetButton("Fire1")&& (Time.time > nextFire))
            {
               if(fireRate==0.35f)
                {
                    _audio.PlayOneShot(shoot);
                }

                Shoot();
           
            }
        }
    }

    void Shoot()
    {
        nombreMissileLances++;
        nextFire = Time.time + fireRate;
        Instantiate(anticorps, weapon.position, weapon.rotation);
        PlayerPrefs.SetInt("anticorpsFired", nombreMissileLances);
        
        
    }

}
