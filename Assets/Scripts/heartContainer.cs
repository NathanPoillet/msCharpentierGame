using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heartContainer : MonoBehaviour
{
    SpriteRenderer sprite;
    public bool destroyed=false;
    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        sprite.color = new Color(255, 255, 255);
    }



    public void Destroyed()
    {
        
        sprite.color = new Color(0, 0, 0);

    }

    public void Repared()
    {

        sprite.color = new Color(255, 255, 255);

    }

}
