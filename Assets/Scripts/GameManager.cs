using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{

    public int meilleurScore;
    public int score;
    public Dna_Life life;
    public GameObject MenuEnd;

    public SpawnVirus spawnVirus;

    public AudioSource _audioSource;
    public AudioClip musicGame;
    public AudioClip explosionMusic;

    public player _player;
    private int virusKilled;

    public ParticleSystem explosion;

    public TextMeshProUGUI InfiniScore;


    private float coef = 1f;
    private int meilleurScoreInf;
    private int meilleurScoreHard;
    private int meilleurScoreSuicide;

    private void Awake()
    {
        Time.timeScale = Difficulty.hardcore;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = Difficulty.volume;
        _audioSource.PlayOneShot(musicGame);
        InfiniScore.gameObject.SetActive(false);
        MenuEnd.SetActive(false);

        virusKilled = PlayerPrefs.GetInt("virusKilled");
        meilleurScore = PlayerPrefs.GetInt("Score");

        score = 0;
        

        StartCoroutine(Coroutine());

    }

    IEnumerator Coroutine()
    {

        bool play = true;
        while (play)
        {
            yield return new WaitForSeconds(0.1f);
            score += 1;

            if (Difficulty.difficulties != 6)
            {

                if (_player.dead == true)
                {
                    _audioSource.PlayOneShot(explosionMusic);
                    life.loser = true;
                    yield return new WaitForSeconds(0.8f);
                    Destroy(_player.gameObject);

                }


                if (life.loser)
                {
                    int game = PlayerPrefs.GetInt("gamePlayed") + 1;
                    PlayerPrefs.SetInt("gamePlayed", game);

                    Time.timeScale = 0.5f;
                    MenuEnd.SetActive(true);
                    InfiniScore.gameObject.SetActive(true);
                    int newScore = PlayerPrefs.GetInt("virusKilled") - virusKilled;
                    InfiniScore.text = "Virus abattu : " + newScore;
                    

                    meilleurScoreHard = PlayerPrefs.GetInt("UHC");
                    meilleurScoreSuicide = PlayerPrefs.GetInt("suicide");


                    switch (Difficulty.difficulties)
                    {
                        case 5:
                            if (score > meilleurScoreHard)
                            {
                                PlayerPrefs.SetInt("UHC", score);
                            }

                            break;
                        case 7:
                            if (score > meilleurScoreSuicide)
                            {
                                PlayerPrefs.SetInt("suicide", score);
                            }
                            break;
                        default:

                            if (score > meilleurScore)
                            {
                                PlayerPrefs.SetInt("Score", score);
                            }
                            break;
                    }
                   


                    if (_player.dead == false)
                    {
                        _audioSource.PlayOneShot(explosionMusic);
                        Instantiate(explosion, life.transform.position, Quaternion.identity);
                    }

                    
                    play = false;
                    yield return new WaitForSeconds(5f);
                    Time.timeScale = 1f;
                    SceneManager.LoadScene(1);
                }

            }


            if (Difficulty.difficulties != 7)
            {
                if (score > 50)
                {
                    if (score > 400)
                    {
                        coef = 2f;
                    }
                    else if (score > 1000)
                    {
                        coef = 4f;
                    }

                    spawnVirus.rateSpawn = spawnVirus.rateSpawn - coef * 0.1f / (float)score;
                }
            }

            if (Difficulty.difficulties == 6)
            {
                if (score == 500)
                {
                    Time.timeScale = 0.5f;
                    MenuEnd.SetActive(true);
                    InfiniScore.gameObject.SetActive(true);
                    

                    int newScore = PlayerPrefs.GetInt("virusKilled") - virusKilled;
                    Debug.Log(PlayerPrefs.GetInt("virusKilled"));
                    Debug.Log(newScore);

                    InfiniScore.text = "Virus abattu : " + newScore;

                    meilleurScoreInf = PlayerPrefs.GetInt("scoreInfini");
                    if (newScore > meilleurScoreInf)
                    {
                        PlayerPrefs.SetInt("scoreInfini", newScore);

                    }

                    play = false;
                    yield return new WaitForSeconds(5f);
                    Time.timeScale = 1f;
                    SceneManager.LoadScene(1);
                }
            }
            
            /*
                        switch (score)
                        {
                            case 100 :
                                spawnVirus.rateSpawn = 0.8f;
                                break;
                            case 200:
                                spawnVirus.rateSpawn = 0.7f;
                                break;
                            case 400:
                                spawnVirus.rateSpawn = 0.6f;
                                break;
                            case 800:
                                spawnVirus.rateSpawn = 0.55f;
                                break;
                            case 1600:
                                spawnVirus.rateSpawn = 0.5f;
                                break;
                            default:
                                break;

                        }*/
           



        }
    }
}





// A faire :

// Modifier score max pour infini
// Réarranger les achievements
// Finir d'implémenter les achievements