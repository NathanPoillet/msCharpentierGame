using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class bestScore : MonoBehaviour
{
    TextMeshProUGUI text;
    
    public int score;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();

        switch (Difficulty.difficulties)
        {
            case 5:
                score = PlayerPrefs.GetInt("UHC");
                text.text = "Best Hardcore : " + score;
                break;
            case 6:
                score = PlayerPrefs.GetInt("scoreInfini");
                text.text = "Best Infini : " + score;
                break;
            case 7:
                score = PlayerPrefs.GetInt("suicide");
                text.text = "Best suicide : " + score;
                break;
            default:
                score = PlayerPrefs.GetInt("Score");
                text.text = "Best : " + score;
                break;
        }
       

       
        
        
    }


}
