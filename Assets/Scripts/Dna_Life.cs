using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dna_Life : MonoBehaviour
{

    public ParticleSystem explosion;
    public List<Sprite> mySprite;
    public int i=0;
    public SpriteRenderer spriteRenderer;

    public bool loser=false;

    // Start is called before the first frame update
    void Start()
    {
        if (Difficulty.difficulties != 5)
        {
            this.GetComponent<SpriteRenderer>().sprite = mySprite[0];
            i = 1;
        } else
        {
            this.GetComponent<SpriteRenderer>().sprite = mySprite[3];
            i = mySprite.Count;
        }
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(Difficulty.difficulties != 6)
        {
            if (collision.gameObject.tag == "virus")
            {
                foreach (ContactPoint2D contact in collision.contacts)
                {
                    GetComponent<AudioSource>().Play();
                    Instantiate(explosion, contact.point, Quaternion.identity);
                }

                if (i == mySprite.Count)
                {
                    loser = true;
                }
                else
                {
                    spriteRenderer.sprite = mySprite[i];
                    i += 1;
                }
            }
        }
           
    }

    public void Ciseaux()
    {
        if (Difficulty.difficulties != 5)
        {
            if (i != 1)
            {
                i = i - 2;
                spriteRenderer.sprite = mySprite[i];
                i = i + 1;
            }
        }
        
    }
}
