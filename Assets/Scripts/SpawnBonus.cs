using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBonus : MonoBehaviour
{

    private GameObject newHeal;
    public List<Sprite> spriteList;

    public float rateSpawn = 5f;

    private int rdmHeal;
    public bonus _ciseaux;
    public bonus fire;
    public bonus bombe;
    public heart _heart;

    private void Start()
    {
        StartCoroutine(SpawnEntities());

    }




    IEnumerator SpawnEntities()
    {
        yield return new WaitForSeconds(0f);
        while (true)
        {
            Invoke("SpawnHeal", 0f);

            yield return new WaitForSeconds(rateSpawn);
        }
    }


    private void SpawnHeal()
    {
        rdmHeal = (int)Random.Range(0, 4);
        float randomSpawnZoneHeal = Random.Range(-9f, 9f);
        Vector3 spawnPosition = new Vector3(20, randomSpawnZoneHeal, 0f);

        if (rdmHeal == 0)
        {
            newHeal = Instantiate(_heart.gameObject, spawnPosition, Quaternion.identity);
        }
        else if (rdmHeal == 1)
        {
            newHeal = Instantiate(_ciseaux.gameObject, spawnPosition, Quaternion.identity);
        }
        else if (rdmHeal == 2)
        {
            newHeal = Instantiate(fire.gameObject, spawnPosition, Quaternion.identity);
        }
        else
        {
            newHeal = Instantiate(bombe.gameObject, spawnPosition, Quaternion.identity);
        }
    }

}
