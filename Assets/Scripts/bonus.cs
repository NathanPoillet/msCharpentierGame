using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonus : MonoBehaviour
{
    int speed =15;
    void Start()
    {
       
    }
    private void OnEnable()
    {
        GameObject[] anticorps = GameObject.FindGameObjectsWithTag("anticorps");
        GameObject[] otherObjects = GameObject.FindGameObjectsWithTag("virus");

        foreach (GameObject obj in anticorps)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

        foreach (GameObject obj in otherObjects)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos;
        pos = transform.position;
        pos.x -= Time.deltaTime * speed;
        transform.position = pos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "player"|| collision.gameObject.tag == "DNA")
        {
            if (collision.gameObject.tag == "player")
            {
                int acquired = PlayerPrefs.GetInt("bonusAcquired") + 1;
                PlayerPrefs.SetInt("bonusAcquired", acquired);
            }
            GetComponent<AudioSource>().Play();
            Destroy(gameObject);
        } 

    }
}
