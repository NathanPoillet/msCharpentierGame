using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Difficulty 
{
    public static int difficulties { get; set; } = 1;
    

    public static float volume = 1;

    public static float hardcore = 1f;
}
