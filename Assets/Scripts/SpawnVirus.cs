using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnVirus : MonoBehaviour
{
    private GameObject ennemy;
    private GameObject newEnnemy;
    public List<Sprite> spriteList;

    public float rateSpawn =1f;

    private int rdm;
    public List<virusmoving> viruses;

    // Update is called once per frame
    private void Start()
    {
        if (Difficulty.difficulties == 7 )
        {
            rateSpawn = 0.6f;
        } else if( Difficulty.difficulties == 6)
        {
            rateSpawn = 0.5f;

        }
        StartCoroutine(SpawnEntities());
        /*InvokeRepeating("SpawnNewEnnemy", 0f, rateSpawn);
        InvokeRepeating("SpawnHeal", 0f, 11f);*/

    }



    IEnumerator SpawnEntities()
    {
        yield return new WaitForSeconds(0f);
        while (true)
        {
            Invoke("SpawnNewEnnemy",0f);
            
            yield return new WaitForSeconds(rateSpawn);
        }
    }


    private void SpawnNewEnnemy()
    {
         rdm = (int)Random.Range(0,3);
        float randomSpawnZone = Random.Range(-9f, 9f);
        Vector3 spawnPosition = new Vector3(20, randomSpawnZone, 0f);
        newEnnemy = Instantiate(viruses[rdm].gameObject, spawnPosition, Quaternion.identity);
        
    }


}
