using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anticorpsShoot : MonoBehaviour
{
    public float speed = 60f;

    private void OnEnable()
    {
        GameObject[] ciseaux = GameObject.FindGameObjectsWithTag("ciseaux");

        foreach (GameObject obj in ciseaux)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

        GameObject[] bombe = GameObject.FindGameObjectsWithTag("bombe");

        foreach (GameObject obj in bombe)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

        GameObject[] fire = GameObject.FindGameObjectsWithTag("fire");

        foreach (GameObject obj in fire)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

        GameObject[] heart = GameObject.FindGameObjectsWithTag("heart");

        foreach (GameObject obj in heart)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
    public void Update()
    {
        Vector3 pos;
        pos = transform.position;
        pos.x += Time.deltaTime * speed;
        transform.position = pos;

        if (transform.position.x > 20)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "virus" || collision.gameObject.tag == "ciseaux" || collision.gameObject.tag == "heart") {
            Destroy(gameObject);
        }

    }


}
