using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ChangeDifficulty : MonoBehaviour
{

    public TextMeshProUGUI text;
    private int difficulty;
    public void Facile()
    {
        difficulty = 1;
        Difficulty.difficulties = 1;
        Difficulty.hardcore = 1f;
        StartCoroutine(Coroutine());
    }
    public void Moyen()
    {
        difficulty = 2;
        Difficulty.difficulties = 2;
        Difficulty.hardcore = 1f;
        StartCoroutine(Coroutine());
    }
    public void Difficle()
    {
        difficulty = 3;
        Difficulty.difficulties = 3;
        Difficulty.hardcore = 1f;
        StartCoroutine(Coroutine());
    }

    public void Impossible()
    {
        difficulty = 4;
        Difficulty.difficulties = 3;
        Difficulty.hardcore = 1.5f;
        StartCoroutine(Coroutine());
    }


    public void Hardcore()
    {
        difficulty = 5;
        Difficulty.difficulties = 5;
        Difficulty.hardcore = 1f;
        StartCoroutine(Coroutine());
    }


    public void Infini()
    {
        difficulty = 6;
        Difficulty.difficulties = 6;
        Difficulty.hardcore = 1f;
        StartCoroutine(Coroutine());
    }

    public void Suicide()
    {
        difficulty = 7;
        Difficulty.difficulties = 7;
        Difficulty.hardcore = 1f;
        StartCoroutine(Coroutine());
    }




    IEnumerator Coroutine()
    {
        text.gameObject.SetActive(true);

        switch (difficulty)
        {
            case 1:
                text.text = "La difficult� a �t� chang� en : Facile";
                break;
            case 2:
                text.text = "La difficult� a �t� chang� en : Moyen";
                break;
            case 3:
                text.text = "La difficult� a �t� chang� en : Difficile";
                break;
            case 4:
                text.text = "La difficult� a �t� chang� en : Impossible";
                break;

                //implementer les modes suivant

                // Une vie, aucune erreur ou c'est fini.
            case 5:
                text.text = "La difficult� a �t� chang� en : mode Hardcore";
                break;

                // Ne peut plu tirer et ne prends pas de d�gat au toucher.
                // Ne peut tuer les virus que en les percutant
            case 6:
                text.text = "La difficult� a �t� chang� en : mode infini"; ;
                break;
                // ne pas prendre en compte le score mais juste le nombre de virus tu�
                // Finir le jeu au bout de 1000 de score -> garder en m�moire le nombre de virus tu� en ce temps donn�
            case 7:
                text.text = "La difficult� a �t� chang� en : Op�ration Suicide";
                break;
            default:
                break;

        }
        yield return new WaitForSeconds(0.4f);
        if (text.gameObject.activeSelf == false)
        {
           
        } else
        {
            
            
            text.gameObject.SetActive(false);
            StopAllCoroutines();

        }
    }
}
