using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class virusmoving : MonoBehaviour
{
    public float speed;
    public ParticleSystem explosion;

    // Start is called before the first frame update
    void Start()
    {
        if (Difficulty.difficulties == 1)
        {
            speed = Random.Range(10, 14);
        } else if (Difficulty.difficulties == 2)
        {
            speed = Random.Range(16, 20);
        } else if (Difficulty.difficulties == 2)
        {
            speed = Random.Range(18, 23);
        } else
        {
            speed = Random.Range(15, 20);
        }
        
    }


    private void OnEnable()
    {
        GameObject[] otherObjects = GameObject.FindGameObjectsWithTag("ciseaux");

        foreach (GameObject obj in otherObjects)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

        GameObject[] virus = GameObject.FindGameObjectsWithTag("virus");

        foreach (GameObject obj in virus)
        {
            Physics2D.IgnoreCollision(obj.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 pos;
        pos = transform.position;
        pos.x -= Time.deltaTime*speed;
        transform.position = pos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if (collision.gameObject.tag !="ciseaux"  && collision.gameObject.tag != "virus")
        {
            if (collision.gameObject.tag == "anticorps" || collision.gameObject.tag == "player")
            {
                int killed = PlayerPrefs.GetInt("virusKilled") + 1;
                PlayerPrefs.SetInt("virusKilled", killed);
            }
            
            Instantiate(explosion, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
       
    }
}
