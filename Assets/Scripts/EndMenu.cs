using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenu : MonoBehaviour
{
   
    public void Play()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
    }

    public void Resume()
    {
        Time.timeScale = 1f;
    }

    public void Quit()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }

    public void Reset()
    {
        PlayerPrefs.DeleteAll();
    }
}
