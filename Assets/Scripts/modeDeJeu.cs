using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class modeDeJeu : MonoBehaviour
{
    TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();

        switch (Difficulty.difficulties)
        {
            case 1:
                text.text = "Facile";
                break;
            case 2:
                text.text = "Moyen";
                break;
            case 3:
                text.text = "Difficile";
                break;
            case 4:
                text.text = "Impossible";
                break;

            case 5:
                text.text = "Hardcore";
                break;
            case 6:
                text.text = "Infini";
                break;
            case 7:
                text.text = " Opération Suicide";
                break;
            default:
                break;
        }

    }


}
