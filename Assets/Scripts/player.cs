using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{

    public GameObject pauseMenu;
    public Dna_Life life;
    float maxVelocity = 12f;

    public int lifePlayer;

    public List<heartContainer> hearts;

    public bool dead;

    public Animator animator;
    public shooting weapon;
    public ParticleSystem explosion;

    public AudioSource _audio;
    public AudioClip explode;
    public AudioClip heart;
    public AudioClip bonus;

    void Start()
    {
        dead = false;
        if (Difficulty.difficulties==5)
        {
            lifePlayer = 1;
            hearts[2].Destroyed();
            hearts[1].Destroyed();
        } else
        {
            lifePlayer = 3;
        }


        if (Difficulty.difficulties == 7)
        {
             maxVelocity = 14f;
        }
        _audio = GetComponent<AudioSource>();
        _audio.volume = Difficulty.volume/2;
        pauseMenu.SetActive(false);
    }

    private void Update()
    {
        if (!dead)
        {
            Vector3 pos = transform.position;

                pos.y = Mathf.Clamp(pos.y + Input.GetAxis("Vertical")* maxVelocity * 1.6f * Time.deltaTime, -9, 9);
          
                pos.x = Mathf.Clamp(pos.x + Input.GetAxis("Horizontal") * maxVelocity * 1.6f * Time.deltaTime, -11, 16);

            transform.position = pos;
        }

        if (Input.GetButtonDown("Escape"))
        {
            // faire un compteur 3 2 1... avant de relancer le jeu
            // faire un menu en plus
            // 
            if (Time.timeScale != 0f)
            {
                Time.timeScale = 0f;
                pauseMenu.SetActive(true);
            }
            else
            {
                Time.timeScale = 1f;
                pauseMenu.SetActive(false);
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ciseaux")
        {
            _audio.PlayOneShot(heart);
            life.Ciseaux();

        }
        else if (collision.gameObject.tag == "virus")
        {
            if(Difficulty.difficulties!=7 && Difficulty.difficulties != 6) // ne peut pas taper lorsqu'en mode suicide
            { 
                animator.SetBool("touched", true);

                lifePlayer -= 1;

                if (lifePlayer >= 0) {
                    hearts[lifePlayer].Destroyed();
                }



                if (lifePlayer <= 0)
                {

                    dead = true;
                    animator.SetBool("dead", true);

                }

                StartCoroutine(Coroutine()); 
            }   

        }
        else if (collision.gameObject.tag == "heart")
        {
            if (Difficulty.difficulties != 5)
            {
                _audio.PlayOneShot(heart);
                if (lifePlayer < 3 && lifePlayer > 0)
                {
                    hearts[lifePlayer].Repared();
                    lifePlayer++;

                }
            }

        }
        else if (collision.gameObject.tag == "fire")
        {
            _audio.PlayOneShot(bonus);
            weapon.fireRate -= 0.30f;
            StartCoroutine(Bonus());
        }
        else if (collision.gameObject.tag == "bombe")
        {
            _audio.PlayOneShot(explode);
            GameObject[] otherObjects = GameObject.FindGameObjectsWithTag("virus");

            foreach (GameObject obj in otherObjects)
            {
                
                Instantiate(explosion, obj.transform.position, Quaternion.identity);
                Destroy(obj);
            }
            
        }


    }


    IEnumerator Coroutine()
    {
        yield return new WaitForSeconds(1f);
        animator.SetBool("touched", false);
        

    }

    IEnumerator Bonus()
    {
        yield return new WaitForSeconds(4f);
        weapon.fireRate = 0.35f;
    }
}


