using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSound : MonoBehaviour
{
    public AudioSource _audio;
    public TextMeshProUGUI volume;
    public Slider slider;

    private void Start()
    {
        volume.text = "volume : 100" ;
        _audio = GetComponent<AudioSource>();
        _audio.volume = 1;
        slider.onValueChanged.AddListener(delegate { ChangeVolume(); });
        slider.value = 100;
    }

  
    public void ChangeVolume()
    {
        _audio.volume =slider.value;
        Difficulty.volume = _audio.volume;
        volume.text = "volume : " + (int)(_audio.volume*100);

    }
}
