using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class points : MonoBehaviour
{
    TextMeshProUGUI text;
    
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TMPro.TextMeshProUGUI>();
        text.text = "Score : 0";
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Score : " + gameManager.score;
    }




}
